FROM mcr.microsoft.com/dotnet/core/sdk:2.2-alpine
 
WORKDIR /app
COPY ./Hello .

RUN ["dotnet", "restore"]
RUN ["dotnet", "build"]

HEALTHCHECK --interval=1m --timeout=3s --retries=5 CMD wget --quiet --tries=1 --spider http://localhost:5000 || exit 1
ENTRYPOINT ["dotnet", "run"]
