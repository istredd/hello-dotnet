﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Hello.Controllers {
    [Route ("/")]
    public class HelloController : ControllerBase {
        private readonly Urls _urls;

        public HelloController (IOptions<Urls> options) {
            _urls = options.Value;
        }

        [HttpGet]
        public async Task<string> Get () {
            var audience = await GetAudience ();

            return $"hello {audience}".Trim();
        }

        private async Task<string> GetAudience () {
            try {
                var httpClient = new HttpClient ();

                var response = await httpClient.GetAsync (_urls.WorldService).ConfigureAwait (false);
                response.EnsureSuccessStatusCode ();

                var message = await response.Content.ReadAsStringAsync ().ConfigureAwait (false);

                return message;

            } catch (Exception ex) {
                return string.Empty;
            }
        }
    }
}