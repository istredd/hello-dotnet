# Hello - frontend

This is a guide how to run **Hello** frontend application on Docker. 

## Building and running docker image locally

Code below will allow you to build container and run locally.
~~~~ 
docker build -t hello .
docker run hello .
~~~~

## Building and uploading containers into the Hub
Once you are allowed to contribute to https://gitlab.com/istredd/hello-dotnet simply use 
**CI/CD -> Pipelines -> Run Pipeline** to start running job manually.  Every contribution to repository triggers job automatically as well. Every merge to master creates new **latest** image which can be used later on https://gitlab.com/istredd/hello-k8s to run k8s cluster or docker-compose

